import requests
import json


url_currentCA = "https://api.covidtracking.com/v1/states/ca/current.json"
response_currentCA = requests.get(url_currentCA)
data_currentCA = response_currentCA.text
parsed_currentCA = json.loads(data_currentCA)

posCasesCurrCA = parsed_currentCA["positiveCasesViral"]
deathCurrCA = parsed_currentCA["death"]


urlCA = "https://api.covidtracking.com/v1/states/ca/daily.json"
 
response_CA= requests.get(urlCA)
dataCA= response_CA.text
parsedCA= json.loads(dataCA)

# date = parsed["date"]

positiveCasesCA = []
deathCA = []

posAvgCA = 0
for i in range(290):
    positiveCasesCA.append(parsedCA[i]["positiveCasesViral"])
    posAvgCA += parsedCA[i]["positiveCasesViral"]

posAvgCA /= 290

# for positiveCase in positiveCasesCA:
#     print(positiveCase)
deathAvgCA = 0
for i in range(200):
    deathCA.append(parsedCA[i]["death"])
    deathAvgCA += parsedCA[i]["death"]

deathAvgCA /= 200

print(posAvgCA)
print(deathAvgCA)

bposCA = False
bdeathCA = False


if(posCasesCurrCA - posAvgCA >= 5000):
    bposCA = True

if(deathCurrCA - deathAvgCA >= 1000):
    bdeathCA = True
    

print(posCasesCurrCA)
print(deathCurrCA)

print(bposCA)
print(bdeathCA)

url_currentTX = "https://api.covidtracking.com/v1/states/tx/current.json"
response_currentTX = requests.get(url_currentTX)
data_currentTX = response_currentTX.text
parsed_currentTX = json.loads(data_currentTX)

poscasesCurrTX = parsed_currentTX["positiveCasesViral"]
deathCurrTX = parsed_currentTX["death"]


urlTX = "https://api.covidtracking.com/v1/states/tx/daily.json"
 
response_TX= requests.get(urlTX)
dataTX= response_TX.text
parsedTX= json.loads(dataTX)

# date = parsed["date"]

positivecasesTX = []
deathTX = []

posAvgTX = 0
for i in range(290):
    positivecasesTX.append(parsedTX[i]["positiveCasesViral"])
    posAvgTX += parsedTX[i]["positiveCasesViral"]

posAvgTX /= 290

# for positivecase in positivecasesTX:
#     print(positivecase)
deathAvgTX = 0
for i in range(200):
    deathTX.append(parsedTX[i]["death"])
    deathAvgTX += parsedTX[i]["death"]

deathAvgTX /= 200

print(posAvgTX)
print(deathAvgTX)

bposTX = False
bdeathTX = False


if(poscasesCurrTX - posAvgTX >= 5000):
    bposTX = True

if(deathCurrTX - deathAvgTX >= 1000):
    bdeathTX = True
 


url_currentMN = "https://api.covidtracking.com/v1/states/MN/current.json"
response_currentMN = requests.get(url_currentMN)
data_currentMN = response_currentMN.text
parsed_currentMN = json.loads(data_currentMN)

poscasesCurrMN = parsed_currentMN["positiveCasesViral"]
deathCurrMN = parsed_currentMN["death"]


urlMN = "https://api.covidtracking.com/v1/states/MN/daily.json"
 
response_MN= requests.get(urlMN)
dataMN= response_MN.text
parsedMN= json.loads(dataMN)

# date = parsed["date"]

positivecasesMN = []
deathMN = []

posAvgMN = 0
for i in range(290):
    positivecasesMN.append(parsedMN[i]["positiveCasesViral"])
    posAvgMN += parsedMN[i]["positiveCasesViral"]

posAvgMN /= 290

# for positivecase in positivecasesMN:
#     print(positivecase)
deathAvgMN = 0
for i in range(200):
    deathMN.append(parsedMN[i]["death"])
    deathAvgMN += parsedMN[i]["death"]

deathAvgMN /= 200

print(posAvgMN)
print(deathAvgMN)

bposMN = False
bdeathMN = False


if(poscasesCurrMN - posAvgMN >= 5000):
    bposMN = True

if(deathCurrMN - deathAvgMN >= 1000):
    bdeathMN = True
    

print(poscasesCurrMN)
print(deathCurrMN)

print(bposMN)
print(bdeathMN)
