from flask import Flask, render_template, request
import requests
import json

app = Flask(__name__)


@app.route('/deaths' , methods=['POST'])
def deaths():
    r = requests.get('https://api.covidtracking.com/v1/states/ca/current.json')
    data = r.text
    parsed = data.loads(data)
    numDeaths = float(parsed["death"])
    return render_template('deaths.html', deaths=numDeaths)

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)   
