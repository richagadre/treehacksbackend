class Fetch {
  async getCurrent(input) {
    // const myKey = "39a9a737b07b4b703e3d1cd1e231eedc";
    const response = await fetch(
      `https://api.covidtracking.com/v1/states/ca/current.json`
    );
    const data = await response.json();
    console.log(data);
    return data;
  }
}
